package com.home.seda.filetransferer.controllers;

import java.io.BufferedReader;
import java.io.File;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.URL;
import java.nio.file.CopyOption;
import java.nio.file.Files;
import java.nio.file.StandardCopyOption;
import java.text.SimpleDateFormat;
import java.time.LocalDateTime;
import java.time.ZonedDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.GregorianCalendar;
import java.util.List;
import java.util.Optional;
import java.util.ResourceBundle;

import org.apache.commons.io.FilenameUtils;

import com.home.seda.filetransferer.App;
import com.home.seda.filetransferer.file.ComputeFileSize;
import com.home.seda.filetransferer.file.CreateFile;
import com.home.seda.filetransferer.file.FileManager;
import com.home.seda.filetransferer.file.FileSpecification;
import com.home.seda.filetransferer.images.CustomImage;
import com.home.seda.filetransferer.os.CheckOperatingSystem;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.fxml.Initializable;
import javafx.scene.control.Alert;
import javafx.scene.control.Alert.AlertType;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonType;
import javafx.scene.control.ComboBox;
import javafx.scene.control.Label;
import javafx.scene.control.MenuItem;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableRow;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.input.KeyCode;
import javafx.scene.input.KeyCodeCombination;
import javafx.scene.input.KeyCombination;

public class AppController implements Initializable {
	@FXML
	MenuItem startTaskManager;
	@FXML
	MenuItem startNotepad;
	@FXML
	Label leftFilePathLabel;
	@FXML
	TextField filePathTextField;
	@FXML
	TableView<FileSpecification> leftTableList;
	@FXML
	TableView<FileSpecification> rightTableList;
	@FXML
	ComboBox<File> leftComboBox;
	@FXML
	ComboBox<File> rightComboBox;
	@FXML
	TableColumn<FileSpecification, ImageView> leftImage;
	@FXML
	TableColumn<FileSpecification, ImageView> rightImage;
	@FXML
	TableColumn<FileSpecification, String> leftName;
	@FXML
	TableColumn<FileSpecification, String> leftSize;
	@FXML
	TableColumn<FileSpecification, String> leftExt;
	@FXML
	TableColumn<FileSpecification, LocalDateTime> leftDate;
	@FXML
	TableColumn<FileSpecification, String> rightName;
	@FXML
	TableColumn<FileSpecification, String> rightSize;
	@FXML
	TableColumn<FileSpecification, String> rightExt;
	@FXML
	TableColumn<FileSpecification, LocalDateTime> rightDate;
	@FXML
	Button deleteFiles;

	private File rightFilePathLabel = null;
	private FileManager fileManager;
	private List<FileSpecification> fileSpec;
	private ComputeFileSize cS;
	private App mainApp;
	private ObservableList<File> fileName;
	private File[] fileArray;
	private ObservableList<CustomImage> imgList = FXCollections.observableArrayList();
	private CopyOption[] options = new CopyOption[] { StandardCopyOption.REPLACE_EXISTING,
			StandardCopyOption.COPY_ATTRIBUTES };
	private DateTimeFormatter dtf = DateTimeFormatter.ofPattern("MM/dd/yyyy HH:mm:ss");
	private SimpleDateFormat sdf = new SimpleDateFormat("MM/dd/yyyy HH:mm:ss");

	public AppController() {

	}

	@Override
	public void initialize(URL arg0, ResourceBundle arg1) {
		this.fileManager = new FileManager();
		this.cS = new ComputeFileSize();
		leftImage.setCellValueFactory(new PropertyValueFactory<FileSpecification, ImageView>("image"));
		rightImage.setCellValueFactory(new PropertyValueFactory<FileSpecification, ImageView>("image"));
		leftName.setCellValueFactory(cellData -> cellData.getValue().firstNameProperty());
		leftSize.setCellValueFactory(cellData -> cellData.getValue().sizeProperty());
		leftExt.setCellValueFactory(cellData -> cellData.getValue().extensionProperty());
		leftDate.setCellValueFactory(cellData -> cellData.getValue().lastTimeModifiedProperty());
		rightName.setCellValueFactory(cellData -> cellData.getValue().firstNameProperty());
		rightSize.setCellValueFactory(cellData -> cellData.getValue().sizeProperty());
		rightExt.setCellValueFactory(cellData -> cellData.getValue().extensionProperty());
		rightDate.setCellValueFactory(cellData -> cellData.getValue().lastTimeModifiedProperty());
		browseThroughtDoubleClick(leftTableList);
		browseThroughtDoubleClick(rightTableList);
		browseThroughtDisk(leftComboBox);
		browseThroughtDisk(rightComboBox);
		setDefaultDirectory();
		setKeyBoardShortcuts();
		//setDeleteKey();
	}
	
	private void setKeyBoardShortcuts(){
		this.startTaskManager.setAccelerator(new KeyCodeCombination(KeyCode.T, KeyCombination.CONTROL_DOWN));
		this.startNotepad.setAccelerator(new KeyCodeCombination(KeyCode.N, KeyCombination.CONTROL_DOWN));
	}

	private void browseThroughtDoubleClick(TableView<FileSpecification> tableView) {
		tableView.setRowFactory(tv -> {
			TableRow<FileSpecification> row = new TableRow<>();
			row.setOnMouseClicked(event -> {
				if (event.getClickCount() == 2 && (!row.isEmpty())) {
					if (row.getItem().getFile().isDirectory()) {
						setDirectory(row.getItem().getFile(), tableView);
					} else {
						//
					}
				}
			});
			return row;
		});
	}

	private void browseThroughtDisk(ComboBox<File> diskView) {
		diskView.setOnAction(event -> {
			File selectedFile = diskView.getSelectionModel().getSelectedItem();
			if (diskView.equals(this.leftComboBox)) {
				setDirectory(selectedFile, this.leftTableList);
			} else {
				setDirectory(selectedFile, this.rightTableList);
			}
		});
	}

	private void setDeleteKey() {
		deleteFiles.getScene().getAccelerators().put(new KeyCodeCombination(KeyCode.D, KeyCombination.CONTROL_DOWN),
				new Runnable() {
					@Override
					public void run() {
						deleteFiles.fire();
					}
				});
	}

	private void setDefaultDirectory() {
		File[] f1 = null;
		if (CheckOperatingSystem.getOperatingSystem().startsWith("Windows")) {
			f1 = File.listRoots();

		} else {
			// linux nebo neco takovyho
			f1 = File.listRoots();
		}

		/*
		 * //File fff1 = new
		 * File("com/home/seda/filetransferer/images/pictures/slozka.png");
		 * //URL url1 = null; try { //url1 = fff1.toURI().toURL(); } catch
		 * (MalformedURLException e) { e.printStackTrace(); }
		 */

		if (f1 != null) {
			this.leftFilePathLabel.setText(f1[0].getAbsolutePath());
			this.rightFilePathLabel = f1[0];
			CustomImage directory = new CustomImage(
					new ImageView(new Image("com/home/seda/filetransferer/images/pictures/slozka.png")));

			this.fileName = FXCollections.observableArrayList();

			boolean hasParent = false;
			boolean initialized = false;
			if (f1[0].listFiles()[0].getParent() != null) {
				hasParent = true;
			}

			this.fileSpec = new ArrayList<>();

			this.fileArray = f1[0].listFiles();
			if (hasParent) {
				for (int i = 0; i < fileArray.length; i++) {

					// podivat se na vylepseni!!!!!!!!!!
					Date date = new Date(fileArray[i].lastModified());
					GregorianCalendar cal = new GregorianCalendar();
					cal.setTime(date);
					ZonedDateTime zdt = cal.toZonedDateTime();
					LocalDateTime ldt = zdt.toLocalDateTime();
					cS.setSize(fileArray[i].length());

					if (initialized == false) {
						this.fileSpec.add(new FileSpecification("..", ldt, cS.getSize(), "<DIR>",
								new File(this.leftFilePathLabel.getText()).getParentFile(), null));
						this.fileManager.addFile(this.fileSpec.get(0));
						initialized = true;
					}

					this.fileSpec.add(new FileSpecification(fileArray[i].getName(), ldt, cS.getSize(),
							FilenameUtils.getExtension(fileArray[i].getAbsolutePath()), fileArray[i], null));

					if (fileArray[i].isDirectory()) {
						fileSpec.get(i + 1).setExtension("<DIR>");
						fileSpec.get(i + 1).setSize("");
						fileSpec.get(i + 1).setImage(directory);
					}
					this.fileManager.addFile(fileSpec.get(i + 1));
					// fileName.add(fileSpec[i].getFile());

				}
			} else {
				for (int i = 0; i < fileArray.length; i++) {

					// podivat se na vylepseni!!!!!!!!!!
					Date date = new Date(fileArray[i].lastModified());
					GregorianCalendar cal = new GregorianCalendar();
					cal.setTime(date);
					ZonedDateTime zdt = cal.toZonedDateTime();
					LocalDateTime ldt = zdt.toLocalDateTime();
					cS.setSize(fileArray[i].length());

					this.fileSpec.add(new FileSpecification(fileArray[i].getName(), ldt, cS.getSize(),
							FilenameUtils.getExtension(fileArray[i].getAbsolutePath()), fileArray[i], null));

					if (fileArray[i].isDirectory()) {
						fileSpec.get(i).setExtension("<DIR>");
						fileSpec.get(i).setSize("");
						fileSpec.get(i).setImage(directory);
					}
					this.fileManager.addFile(fileSpec.get(i));
					// fileName.add(fileSpec[i].getFile());
				}
			}

			this.fileName.addAll(f1);
			this.leftComboBox.setItems(this.fileName);
			this.rightComboBox.setItems(this.fileName);

			this.leftTableList.setItems(this.fileManager.getFiles());
			this.rightTableList.setItems(this.fileManager.getFiles());

			if (this.fileName != null) {
				this.leftComboBox.setValue(this.fileName.get(0));
				this.rightComboBox.setValue(this.fileName.get(0));
			}
			if (!this.fileManager.getFiles().isEmpty())
				this.leftTableList.getSelectionModel().select(0);
			if (!this.fileManager.getFiles().isEmpty())
				this.rightTableList.getSelectionModel().select(0);

		}

	}

	public void setDirectory(File f1, TableView<FileSpecification> tableView) {

		if (tableView.equals(this.leftTableList)) {
			this.leftFilePathLabel.setText(f1.getAbsolutePath());
		} else if (tableView.equals(this.rightTableList)) {
			this.rightFilePathLabel = f1;
		}

		this.fileManager = null;
		this.fileSpec = new ArrayList<>();
		this.fileManager = new FileManager();

		if ((this.fileArray = f1.listFiles()) != null) {
			CustomImage directory = new CustomImage(
					new ImageView(new Image("com/home/seda/filetransferer/images/pictures/slozka.png")));

			// this.fileArray = f1.listFiles();
			boolean hasParent = false;
			boolean initialized = false;
			int numberOfFiles = 0;
			if (new File(this.leftFilePathLabel.getText()).getParentFile() != null) {
				hasParent = true;
			}

			if (hasParent) {
				for (int i = 0; i < this.fileArray.length; i++) {

					// podivat se na vylepseni!!!!!!!!!!
					Date date = new Date(fileArray[i].lastModified());
					GregorianCalendar cal = new GregorianCalendar();
					cal.setTime(date);
					ZonedDateTime zdt = cal.toZonedDateTime();
					LocalDateTime ldt = zdt.toLocalDateTime();
					cS.setSize(fileArray[i].length());

					if (initialized == false) {
						if (tableView.equals(this.leftTableList)) {
							this.fileSpec.add(new FileSpecification("..", ldt, cS.getSize(), "<DIR>",
									new File(this.leftFilePathLabel.getText()).getParentFile(), null));
							this.fileManager.addFile(this.fileSpec.get(0));
							initialized = true;
						} else {
							this.fileSpec.add(new FileSpecification("..", ldt, cS.getSize(), "<DIR>",
									new File(this.rightFilePathLabel.getAbsolutePath()).getParentFile(), null));
							this.fileManager.addFile(this.fileSpec.get(0));
							initialized = true;
						}
					}

					this.fileSpec.add(new FileSpecification(fileArray[i].getName(), ldt, cS.getSize(),
							FilenameUtils.getExtension(fileArray[i].getAbsolutePath()), fileArray[i], null));

					if (fileArray[i].isDirectory()) {
						fileSpec.get(i + 1).setExtension("<DIR>");
						fileSpec.get(i + 1).setSize("");
						fileSpec.get(i + 1).setImage(directory);
					}
					this.fileManager.addFile(fileSpec.get(i + 1));

					// fileName.add(fileSpec[i].getFile());

				}
			} else {
				for (int i = 0; i < fileArray.length; i++) {

					// podivat se na vylepseni!!!!!!!!!!
					Date date = new Date(fileArray[i].lastModified());
					GregorianCalendar cal = new GregorianCalendar();
					cal.setTime(date);
					ZonedDateTime zdt = cal.toZonedDateTime();
					LocalDateTime ldt = zdt.toLocalDateTime();
					cS.setSize(fileArray[i].length());

					this.fileSpec.add(new FileSpecification(fileArray[i].getName(), ldt, cS.getSize(),
							FilenameUtils.getExtension(fileArray[i].getAbsolutePath()), fileArray[i], null));

					if (fileArray[i].isDirectory()) {
						fileSpec.get(i).setExtension("<DIR>");
						fileSpec.get(i).setSize("");
						fileSpec.get(i).setImage(directory);
					}
					this.fileManager.addFile(fileSpec.get(i));
				}
			}

			if (tableView.equals(this.leftTableList)) {
				this.leftTableList.setItems(this.fileManager.getFiles());
				if (!this.fileManager.getFiles().isEmpty())
					leftTableList.getSelectionModel().select(0);
			} else {
				this.rightTableList.setItems(this.fileManager.getFiles());
				if (!this.fileManager.getFiles().isEmpty())
					this.rightTableList.getSelectionModel().select(0);
			}

		} else {

			this.fileSpec.add(new FileSpecification("..", LocalDateTime.now(), cS.getSize(), "<DIR>",
					new File(this.leftFilePathLabel.getText()), null));
			this.fileManager.addFile(this.fileSpec.get(0));
		}
	}

	@FXML
	public void startTaskManager(ActionEvent event) {
		Process p = null;
		StringBuilder sb = new StringBuilder();
		try {
			p = Runtime.getRuntime().exec("taskmgr.exe");
			p.waitFor();

			BufferedReader reader = new BufferedReader(new InputStreamReader(p.getInputStream()));

			String line = "";
			while ((line = reader.readLine()) != null) {
				sb.append(line + "\n");
			}
		} catch (IOException e) {
			e.printStackTrace();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	@FXML
	public void createFile(ActionEvent event) {
		String fileLocation = "";
		System.out.println("Prave vytvarim soubor: Poprve");
		if (this.fileManager != null) {
			System.out.println("Prave vytvarim soubor: HAHAHA");
			File file = this.fileManager.getFiles().get(1).getFile();
			if (file.exists()) {
				fileLocation = file.getParent();
				System.out.println("Prave vytvarim soubor ve slozce:   " + fileLocation);
			}
		}

		CreateFile cfDialog = new CreateFile(this.leftComboBox.getScene().getWindow(), fileLocation);
		cfDialog.showAndWait();

		// musím vytvořit novej soubor a umístit ho do složky, kterou mám právě
		// otevřenou
		this.fileManager.addFile(cfDialog.getNewFileSpec());
	}

	@FXML
	public void startNotepad(ActionEvent evt) {
		try {
			if (CheckOperatingSystem.getOperatingSystem().contains("Windows")) {
				Process p = Runtime.getRuntime().exec("cmd /c start notepad");
			} else {
				System.out.println("Nejaky command pro spusteni geditu na linuxu");
			}
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	@FXML
	public void exitApp(ActionEvent event) {
		Alert alert = new Alert(AlertType.WARNING);
		alert.setTitle("Confirmation Dialog");
		alert.setHeaderText("Do you really want to exit this application?");
		alert.setContentText("Are you ok with this?");

		Optional<ButtonType> result = alert.showAndWait();
		if (result.get() == ButtonType.OK) {
			System.exit(0);
		} else {
			alert.close();
		}
	}

	@FXML
	public void copyFiles(ActionEvent event) {
		System.out.println("Melo by to zkopirovat soubor");
		ObservableList<FileSpecification> selectedFiles = this.leftTableList.getSelectionModel().getSelectedItems();
		for (FileSpecification f : selectedFiles) {
			System.out.println(f.getFileName());
			System.out.println(f.getFileName());
			try {
				Files.copy(f.getFile().toPath(),
						new File((this.rightFilePathLabel.getAbsolutePath() + "/" + f.getFileName())).toPath(),
						StandardCopyOption.REPLACE_EXISTING);
			} catch (IOException io) {
				io.printStackTrace();
			}
			this.rightTableList.getItems().add(f);
		}
	}

	@FXML
	public void moveFiles(ActionEvent event) {
		System.out.println("Melo by to movnout nejaky soubor");
		ObservableList<FileSpecification> selectedFiles = this.leftTableList.getSelectionModel().getSelectedItems();
		for (FileSpecification f : selectedFiles) {
			System.out.println(f.getFileName());
			System.out.println(f.getFileName());
			try {
				Files.move(f.getFile().toPath(),
						new File((this.rightFilePathLabel.getAbsolutePath() + "/" + f.getFileName())).toPath(),
						StandardCopyOption.REPLACE_EXISTING);
			} catch (IOException io) {
				io.printStackTrace();
			}
			this.rightTableList.getItems().add(f);
		}
	}

	@FXML
	public void deleteFiles(ActionEvent event) {
		Alert alert = new Alert(AlertType.CONFIRMATION);
		alert.setTitle("Confirmation Dialog");
		alert.setHeaderText("Do you really want to delete this file / directory?");
		alert.setContentText("Are you ok with this?");

		Optional<ButtonType> result = alert.showAndWait();
		if (result.get() == ButtonType.OK) {
			// ... user chose OK
			ObservableList<FileSpecification> selectedFiles = this.leftTableList.getSelectionModel().getSelectedItems();
			if (selectedFiles != null) {
				for (FileSpecification f : selectedFiles) {
					String path = f.getFile().getAbsolutePath();
					this.leftTableList.getItems().removeIf(file -> file.equals(f));
					this.fileManager.getFiles().removeIf(file -> file.equals(f));
					new File(path).delete();
				}
			}
		} else {
			// ... user chose CANCEL or closed the dialog
			alert.close();
		}

	}

}
