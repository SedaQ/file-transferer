package com.home.seda.filetransferer;

import javafx.application.Application;
import javafx.fxml.FXMLLoader;
import javafx.scene.Parent;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.stage.Stage;

/**
 * Hello world!
 *
 */
public class App extends Application
{
	@Override
	public void start(Stage stage) {
		try {
			Parent root = FXMLLoader.load(getClass().getResource("layouts/AppLayout.fxml"));

	        Scene scene = new Scene(root);

	        stage.setTitle("File transferer");
	        //stage.setMaximized(true);
	        stage.setScene(scene);
	        Image titleIcon = new Image("com/home/seda/filetransferer/images/pictures/file_transfer-618x336.jpg");
	        
	        stage.getIcons().add(titleIcon);
	        stage.show();
		} catch(Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void main(String[] args) {
		launch(args);
	}
}
