package com.home.seda.filetransferer.file;

import java.io.File;
import java.time.LocalDateTime;

import com.home.seda.filetransferer.images.CustomImage;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.beans.property.SimpleStringProperty;
import javafx.beans.property.StringProperty;

public class FileSpecification {

	private ObjectProperty<CustomImage> image;
	private ObjectProperty<File> file;
	private SimpleStringProperty fileName;
	private ObjectProperty<LocalDateTime> lastTimeModified;
	private SimpleStringProperty size;
	private SimpleStringProperty extension;

	public FileSpecification(String fileName, LocalDateTime lastTimeModified, String size, String extension,
			File file,CustomImage image) {
		this.fileName = new SimpleStringProperty(fileName);
		this.lastTimeModified = new SimpleObjectProperty<LocalDateTime>(lastTimeModified);
		this.size = new SimpleStringProperty(size);
		this.extension = new SimpleStringProperty(extension);
		this.file = new SimpleObjectProperty<File>(file);
		this.image = new SimpleObjectProperty<CustomImage>(image);
	}

	public CustomImage getImage() {
		return this.image.get();
	}

	public void setImage(CustomImage image) {
		this.image.set(image);
	}

	public File getFile() {
		return this.file.get();
	}

	public void setFile(File file) {
		this.file.set(file);
	}

	public String getFileName() {
		return fileName.get();
	}

	public void setFileName(String fileName) {
		this.fileName.set(fileName);
	}

	public LocalDateTime getLastTimeModified() {
		return lastTimeModified.get();
	}

	public void setLastTimeModified(LocalDateTime lastTimeModified) {
		this.lastTimeModified.set(lastTimeModified);
	}

	public String getSize() {
		return size.get();
	}

	public void setSize(String size) {
		this.size.set(size);
	}

	public String getExtension() {
		return extension.get();
	}

	public void setExtension(String extension) {
		this.extension.set(extension);
	}

	public ObjectProperty<CustomImage> imageProperty() {
		return this.image;
	}

	public ObjectProperty<File> fileProperty() {
		return this.file;
	}

	public StringProperty firstNameProperty() {
		return this.fileName;
	}

	public ObjectProperty<LocalDateTime> lastTimeModifiedProperty() {
		return this.lastTimeModified;
	}

	public StringProperty sizeProperty() {
		return this.size;
	}

	public StringProperty extensionProperty() {
		return this.extension;
	}

}
