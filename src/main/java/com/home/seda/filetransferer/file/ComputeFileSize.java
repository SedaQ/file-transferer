package com.home.seda.filetransferer.file;

public class ComputeFileSize {

	private String size;
	private static final long KILOBYTE = 1_024;
	private static final long MEGABYTE = 1_048_576;
	private static final long GIGABYTE = 1_073_741_824;

	public ComputeFileSize() {
		this.size = "";
	}

	public void setSize(long size) {
		if (size < KILOBYTE) {
			this.size = String.valueOf(size) + " B";
		} else if (size > KILOBYTE && size < MEGABYTE) {
			this.size = String.valueOf(size / KILOBYTE) + " kB";
		} else if (size > MEGABYTE && size < GIGABYTE) {
			this.size = String.valueOf(size / MEGABYTE) + " MB";
		} else if (size > GIGABYTE) {
			this.size = String.valueOf(size / GIGABYTE) + " GB";
		}
	}

	public String getSize() {
		return this.size;
	}
}
