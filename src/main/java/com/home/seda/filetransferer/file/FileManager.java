package com.home.seda.filetransferer.file;

import javafx.collections.FXCollections;
import javafx.collections.ObservableList;

public class FileManager {

	private ObservableList<FileSpecification> files = FXCollections.observableArrayList();

	public FileManager(){
		super();
	}

	public ObservableList<FileSpecification> getFiles() {
		return this.files;
	}

	public void setFiles(ObservableList<FileSpecification> files) {
		this.files = files;
	}

	public void addFile(FileSpecification file) {
		this.files.add(file);
	}

	public void removeFile(FileSpecification file) {
		this.files.remove(file);
	}

}
