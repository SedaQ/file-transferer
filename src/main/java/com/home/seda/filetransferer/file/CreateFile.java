package com.home.seda.filetransferer.file;

import java.io.File;
import java.io.IOException;
import java.text.ParseException;
import java.time.LocalDateTime;

import org.apache.commons.io.FilenameUtils;

import javafx.beans.value.ChangeListener;
import javafx.beans.value.ObservableValue;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.GridPane;
import javafx.scene.layout.VBox;
import javafx.stage.Modality;
import javafx.stage.Stage;
import javafx.stage.StageStyle;
import javafx.stage.Window;

public class CreateFile extends Stage {

	private File file;
	private FileSpecification newFileSpec;
	private String fileLocation;

	public CreateFile(Window window, String fileLocation) {
		setTitle("New file");
		setWidth(300);
		setHeight(175);

		initStyle(StageStyle.UTILITY);
		initModality(Modality.WINDOW_MODAL);
		initOwner(window);
		setScene(vytvorScenu());
		this.file = null;
		this.fileLocation = fileLocation;
	}

	public File getFile() {
		return this.file;
	}

	public FileSpecification getNewFileSpec() {
		return this.newFileSpec;
	}

	private Scene vytvorScenu() {
		VBox box = new VBox();
		box.setAlignment(Pos.CENTER);
		box.setSpacing(20);

		// Mřížka s TextFieldy a Labely
		GridPane grid = new GridPane();
		grid.setAlignment(Pos.CENTER);
		// grid.setPadding(new Insets(10));
		grid.setHgap(10);
		grid.setVgap(10);

		// Komponenty
		TextField fileNameTextField = new TextField();
		fileNameTextField.setMinWidth(100);
		fileNameTextField.setPrefWidth(100);
		fileNameTextField.setMaxWidth(400);
		// add listner
		fileNameTextField.textProperty().addListener(new ChangeListener<String>() {
			@Override
			public void changed(ObservableValue<? extends String> observable, String oldValue, String newValue) {
				fileNameTextField.setPrefWidth(fileNameTextField.getText().length() * 7); // why
																							// 7?
																							// Totally
																							// trial
																							// number.
			}
		});
		Label fileLabel = new Label("File name");

		grid.add(fileLabel, 0, 0);
		grid.add(fileNameTextField, 1, 0);

		// Tlačítko
		Button button = new Button("OK");
		button.setOnAction(new EventHandler<ActionEvent>() {

			@Override
			public void handle(ActionEvent e) {
				try {
					if (!fileNameTextField.getText().trim().equals("")) {
						file = new File(fileLocation + "/" + fileNameTextField.getText());
						if (!file.exists()) {
							try {
								file.createNewFile();
							} catch (IOException e1) {
								e1.printStackTrace();
							}
						}
						ComputeFileSize cfs = new ComputeFileSize();
						cfs.setSize(file.length());
						newFileSpec = new FileSpecification(file.getName(), LocalDateTime.now(), cfs.getSize(),
								FilenameUtils.getExtension(file.getAbsolutePath()), file,null);
						hide();
					}
				} catch (IllegalArgumentException ex) {
					System.out.println("Error in parsing text from FileName: " + ex.getMessage());
				}
			}
		});

		box.getChildren().addAll(grid, button);
		return new Scene(box);
	}
}
