package com.home.seda.filetransferer.os;

public class CheckOperatingSystem extends Object{

	public CheckOperatingSystem() {
		super();
	}

	public static String getOperatingSystem() {
		return System.getProperty("os.name");
	}
	
}
