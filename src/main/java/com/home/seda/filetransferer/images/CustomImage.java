package com.home.seda.filetransferer.images;

import javafx.beans.property.ObjectProperty;
import javafx.beans.property.SimpleObjectProperty;
import javafx.scene.image.ImageView;

public class CustomImage {
	private ObjectProperty<ImageView> image;

	public CustomImage(ImageView img) {
		this.image = new SimpleObjectProperty<ImageView>(img);
	}

	public void setImage(ImageView value) {
		this.image.set(value);
	}

	public ImageView getImage() {
		return this.image.get();
	}

	public ObjectProperty<ImageView> imageProperty() {
		return this.image;
	}

}
