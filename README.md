File transferer is a light version of Total Commander which supports basic actions with files and directories as Edit, Move, Delete, Create. Enables to start Notepad, Task Manager etc. via Help button.

![FileTransferer.png](https://bitbucket.org/repo/G4paq7/images/1040114033-FileTransferer.png)